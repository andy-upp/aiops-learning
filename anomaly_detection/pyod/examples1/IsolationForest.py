import pandas as pd
import numpy as np
from numpy import percentile
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme();
sns.set_style("darkgrid", {"font.sans-serif": ['simhei', 'Droid Sans Fallback']})
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import MinMaxScaler

from pyod.models.abod import ABOD
from pyod.models.cblof import CBLOF
from pyod.models.feature_bagging import FeatureBagging
from pyod.models.hbos import HBOS
from pyod.models.iforest import IForest
from pyod.models.knn import KNN
from pyod.models.lof import LOF
from pyod.models.mcd import MCD
from pyod.models.ocsvm import OCSVM
from pyod.models.pca import PCA
from pyod.models.lscp import LSCP
import warnings

warnings.filterwarnings("ignore")
df=pd.read_csv("order_num.csv")
from pyod.models.lscp import LSCP




x = np.concatenate((np.random.normal(loc=-2, scale=.5, size=500), np.random.normal(loc=2, scale=.5, size=500)))

isolation_forest = IsolationForest(n_estimators=100)
isolation_forest.fit(x.reshape(-1, 1))
xx = np.linspace(-6, 6, 100).reshape(-1, 1)
anomaly_score = isolation_forest.decision_function(xx)
outlier = isolation_forest.predict(xx)

plt.figure(figsize=(10, 8))
plt.subplot(2, 1, 1)
plt.hist(x)
plt.xlim([-5, 5])

plt.subplot(2, 1, 2)
plt.plot(xx, anomaly_score, label='异常值分数')
plt.fill_between(xx.T[0], np.min(anomaly_score), np.max(anomaly_score), where=outlier == -1, color='r', alpha=.4,
                 label='异常值区域')
plt.legend()
plt.ylabel('异常值分数')
plt.xlabel('x')
plt.xlim([-5, 5])
plt.show()