import pandas as pd
import numpy as np
from numpy import percentile
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme();
sns.set_style("darkgrid", {"font.sans-serif": ['simhei', 'Droid Sans Fallback']})
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import MinMaxScaler

from pyod.models.abod import ABOD
from pyod.models.cblof import CBLOF
from pyod.models.feature_bagging import FeatureBagging
from pyod.models.hbos import HBOS
from pyod.models.iforest import IForest
from pyod.models.knn import KNN
from pyod.models.lof import LOF
from pyod.models.mcd import MCD
from pyod.models.ocsvm import OCSVM
from pyod.models.pca import PCA
from pyod.models.lscp import LSCP
import warnings

warnings.filterwarnings("ignore")
df=pd.read_csv("order_num.csv")

print (df)


print("在 num_people 列中总共有 %d 个空值." % df['num_people'].isnull().sum())
print("在 num_order 列中总共有 %d 个空值." % df['num_order'].isnull().sum())
df=df[~df.num_people.isnull()==True]
df=df[~df.num_order.isnull()==True]
print("删除缺失值以后记录总数:",len(df))




print(df.num_people.describe())
print()
print(df.num_order.describe())
plt.figure(figsize=(15,8), dpi=80)
plt.subplot(221)
sns.distplot(df['num_people'])

plt.subplot(222)
sns.distplot(df['num_order'])